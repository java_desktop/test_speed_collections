package main;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

public class Tester {
	
	enum NameMethod{
		ADD,
		GET,
		SET,
		SIZE,
		TO_ARRAY,
		IS_EMPTY,
		REMOVE
	};
	
	private int N;
	private HashMap<NameMethod, Long> arrayListTimed;
	private HashMap<NameMethod, Long> linkedListTimed;

	
	public Tester() {
		N = 50000;
		arrayListTimed = new HashMap();
		linkedListTimed = new HashMap();
	}
	
	private void testArrayList() {
		ArrayList<Integer> test;
		Date startTime;
		Date finishTime;
		
		// add
		test = new ArrayList();
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.add(i);
		}
		finishTime = new Date();
		arrayListTimed.put(NameMethod.ADD, finishTime.getTime() - startTime.getTime());
		
		// get
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.get(i);
		}
		finishTime = new Date();
		arrayListTimed.put(NameMethod.GET, finishTime.getTime() - startTime.getTime());
		
		// set
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.set(i, i);
		}
		finishTime = new Date();
		arrayListTimed.put(NameMethod.SET, finishTime.getTime() - startTime.getTime());
		
		// size
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.size();
		}
		finishTime = new Date();
		arrayListTimed.put(NameMethod.SIZE, finishTime.getTime() - startTime.getTime());

		// toArray
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.toArray();
		}
		finishTime = new Date();
		arrayListTimed.put(NameMethod.TO_ARRAY, finishTime.getTime() - startTime.getTime());
		
		// is_empty
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.isEmpty();
		}
		finishTime = new Date();
		arrayListTimed.put(NameMethod.IS_EMPTY, finishTime.getTime() - startTime.getTime());
		
		// remove
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.remove(0);
		}
		finishTime = new Date();
		arrayListTimed.put(NameMethod.REMOVE, finishTime.getTime() - startTime.getTime());
		
	}
	
	private void testLinkedList() {
		LinkedList<Integer> test;
		Date startTime;
		Date finishTime;
		
		// add
		test = new LinkedList();
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.add(i);
		}
		finishTime = new Date();
		linkedListTimed.put(NameMethod.ADD, finishTime.getTime() - startTime.getTime());
		
		// get
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.get(i);
		}
		finishTime = new Date();
		linkedListTimed.put(NameMethod.GET, finishTime.getTime() - startTime.getTime());
		
		// set
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.set(i, i);
		}
		finishTime = new Date();
		linkedListTimed.put(NameMethod.SET, finishTime.getTime() - startTime.getTime());
		
		// size
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.size();
		}
		finishTime = new Date();
		linkedListTimed.put(NameMethod.SIZE, finishTime.getTime() - startTime.getTime());

		// toArray
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.toArray();
		}
		finishTime = new Date();
		linkedListTimed.put(NameMethod.TO_ARRAY, finishTime.getTime() - startTime.getTime());
		
		// is_empty
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.isEmpty();
		}
		finishTime = new Date();
		linkedListTimed.put(NameMethod.IS_EMPTY, finishTime.getTime() - startTime.getTime());
		
		// remove
		startTime = new Date();
		for(int i = 0; i < N; i++) {
			test.remove(0);
		}
		finishTime = new Date();
		linkedListTimed.put(NameMethod.REMOVE, finishTime.getTime() - startTime.getTime());
		
	}
	
	private void printResults() {
		
		 System.out.format("| %-10s | %-30s | %-20s | %-20s |%n", "Method", "Counter calls", "ArrayList<T>(ms)", "LinkedList<T>(ms)");
		 System.out.format("| %-10s | %-30d | %-20d | %-20d |%n", "add", N, arrayListTimed.get(NameMethod.ADD), linkedListTimed.get(NameMethod.ADD));
		 System.out.format("| %-10s | %-30d | %-20d | %-20d |%n", "get", N, arrayListTimed.get(NameMethod.GET), linkedListTimed.get(NameMethod.GET));
		 System.out.format("| %-10s | %-30d | %-20d | %-20d |%n", "set", N, arrayListTimed.get(NameMethod.SET), linkedListTimed.get(NameMethod.SET));
		 System.out.format("| %-10s | %-30d | %-20d | %-20d |%n", "size", N, arrayListTimed.get(NameMethod.SIZE), linkedListTimed.get(NameMethod.SIZE));
		 System.out.format("| %-10s | %-30d | %-20d | %-20d |%n", "toArray", N, arrayListTimed.get(NameMethod.TO_ARRAY), linkedListTimed.get(NameMethod.TO_ARRAY));
		 System.out.format("| %-10s | %-30d | %-20d | %-20d |%n", "isEmpty", N, arrayListTimed.get(NameMethod.IS_EMPTY), linkedListTimed.get(NameMethod.IS_EMPTY));
		 System.out.format("| %-10s | %-30d | %-20d | %-20d |%n", "remove", N, arrayListTimed.get(NameMethod.REMOVE), linkedListTimed.get(NameMethod.REMOVE));

	}
	
	public void testAndPrintResults() {
		testLinkedList();
		testArrayList();
		printResults();
	}

}
